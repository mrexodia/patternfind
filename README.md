#patternfind

This is a very simple project aimed at binary pattern finding inside a local buffer. It also supports applying binary masks to data and simple search & replacing.